[Site Web du Projet](http://touchelibre.fr)

# Support PC portable : Iota-01

![Logo Projet](01_Système/05_Logo/Logo_ToucheLibre_V3.png)


## Présentation du Projet

![3D de l’objet](02_Mécanique/03_Plan_3D/Plan_3D_Support_iota.png)
![Photo de l’objet fini](03_Fabrication/02_Fabri_Méca/Photos/P1010583.JPG)

Le projet Iota-01 a pour but de proposer un support pour PC portable de manière à améliorer l’ergonomie quotidien lors de l’utilisation d’un PC portable en association avec le clavier TouLi.


## Avancement

Voici le reste à faire et l’intention final du projet. L’avancement est donné entre accolade {x %}.

* [x] {100%} Spécification générale
* [x] {100%} Dessin du boitier en 3D
* [x] {100%} Fabrication du prototype
* [ ] {0%} Documentation pour la fabrication


## Les dossiers

Le dossier **01_Système** contient tous les éléments architecturant, la spécification générale du produit et toutes les informations qui consernent potentiellement tous les métiers

Le dossier **02_Mécanique** servent à la conception mécanique du produit.

Le dossier **03_Fabrication** est destiné à ceux qui veulent  simplement fabriquer le produit sans trop réfléchir au détail de la conception. Ce dossier est un peu l’équivalent de la rubrique «Téléchargement» pour un projet de type logiciel libre. Mais évidement, un objet libre ne se fabrique pas aussi facilement qu’un simple téléchargement. La philosophie de ce dossier est d’expliquer le plus simplement possible comment fabriquer l’objet.


## Licence

La licence attribuée est différente selon le métier considéré. Aussi, il y a une licence différente dans chaque dossier selon la répartition suivante :

| Dossier         | Licence (SPDX identifier) |
| --------------- | ------------------------- |
| 01_Système      | CC-BY-SA-4.0-or-later     |
| 02_Mécanique    | CERN-OHL-S-2.0-or-later   |
| 03_Fabrication  | CC-BY-SA-4.0-or-later     |

Voir <https://spdx.org/licenses/>


## À Propos de l’Auteur

__Lilian Tribouilloy :__

* __Formation :__ Ingénieur en électronique, diplômé de l’[ENSEA](https://www.ensea.fr/fr) en 2004.
* __Métier :__ Concepteur électronique spécialisé dans les radiofréquences et la CEM (Compatibilité ÉlectroMagnétique).
* __Exemples de produit conçu dans le cadre professionnel :__ Émetteur et Réémetteur pour la télévision numérique ; Amplificateur de puissance classe AB pour une modulation OFDM ; Calculateur entrées/sorties pour camion ; Tableau de bord pour véhicule spéciaux ; Boîtier de télématique pour la gestion de flotte.
* __Objet Libre conçu :__ [ToucheLibre](http://touchelibre.fr/), un clavier d’ordinateur ergonomique en bois. Alliant l’esthétique à l’utile, il s’inscrit dans des valeurs de liberté, d’écologie et de santé.


